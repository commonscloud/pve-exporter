# Proxmox PVE Exporter

Based on https://github.com/prometheus-pve/prometheus-pve-exporter

## Configure

Create an API Token on Proxmox Auth and configure _docker-compose.yml_

prometheus.yml
```
...
pve-exporter-prod:
    image: registry.commonscloud.coop/pve-exporter
    restart: always
    logging:
      driver: none
    environment:
      - PVE_USER=${PVE_USER_PROD}
      - PVE_TOKEN_NAME=${PVE_TOKEN_NAME_PROD}
      - PVE_TOKEN_VALUE=${PVE_TOKEN_VALUE_PROD}
      - PVE_VERIFY_SSL=false
    networks:
      - monitor
...
```
